package com.drivetry.acrepair

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.drivetry.acrepair.adapter.ListAdapter
import com.drivetry.acrepair.adapter.TitleImageAdapter
import com.drivetry.acrepair.model.AcRepiarModel
import com.drivetry.acrepair.model.ListOfAcRepair
import com.drivetry.acrepair.model.ListOfImages
import com.drivetry.acrepair.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.list_ac_repair.*
import kotlinx.android.synthetic.main.list_ac_repair_tech.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        setAcRepairList()
    }

    private fun setAcRepairList() {
        viewModel.setAcRepairList(this)
        viewModel.getAcRepairList()
        getAcRepairList()
    }

    private fun getAcRepairList() {
        viewModel.getListOfRepairLiveData.observe(this, Observer {
            setData(it)
        })
    }

    private fun setData(acModel: AcRepiarModel?) {
        statusTv.text = acModel?.status
        acModel?.repairDetails?.let { setUpCategoryRv(it) }
        acModel?.listOfImageProb?.let { setTitleImageAdapter(it) }

        technicianRating.rating = 5.0F
    }

    private fun setUpCategoryRv(acRepiarModel:  List<ListOfAcRepair>) {
        val acListAdapter =
            ListAdapter(acRepiarModel, this)

        disableNestedScrollingToAvoidConflictWithScrollView()
        serviceListRv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = acListAdapter
        }
    }

    private fun disableNestedScrollingToAvoidConflictWithScrollView() {
        serviceListRv.isNestedScrollingEnabled = false
    }

    private fun setTitleImageAdapter(titleImage: List<ListOfImages>){

        val acTitleImageAdapter =
            TitleImageAdapter(titleImage)

        titleImageRv.isNestedScrollingEnabled = false
        titleImageRv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = acTitleImageAdapter
        }

    }
}
