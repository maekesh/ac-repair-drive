package com.drivetry.acrepair.model

import android.graphics.drawable.Drawable

data class AcRepiarModel(
    val status: String,
    val repairDetails: List<ListOfAcRepair>,
    val listOfImageProb: List<ListOfImages>
)

data class ListOfAcRepair(val nameAC: String, val listofRepair: List<String>, val price: String )

data class ListOfImages(val title: String, val listOfImage: List<Drawable>)
