package com.drivetry.acrepair.usecase

import android.content.Context
import android.graphics.drawable.Drawable
import com.drivetry.acrepair.R
import com.drivetry.acrepair.model.AcRepiarModel
import com.drivetry.acrepair.model.ListOfAcRepair
import com.drivetry.acrepair.model.ListOfImages


class MainActivityUseCase {

    lateinit var acModel: AcRepiarModel

    fun setAcRepairModel(context: Context) {

        var listofRepair1 = mutableListOf<String>()

        listofRepair1.add(context.getString(R.string.window_ac_repair))
        listofRepair1.add(context.getString(R.string.not_starting))
        listofRepair1.add(context.getString(R.string.water_leakage))

        var listOfRepair2 = mutableListOf<String>()
        listOfRepair2.add(context.getString(R.string.split_ac_repair))
        listOfRepair2.add(context.getString(R.string.not_starting))

        var acRepair1 = ListOfAcRepair("Ac Repair", listofRepair1, "600")
        var acRepair2 = ListOfAcRepair("Ac Repair", listOfRepair2, "600")

        var listOfAcRepair = mutableListOf<ListOfAcRepair>()
        listOfAcRepair.add(acRepair1)
        listOfAcRepair.add(acRepair2)

        /*Images part*/

        val listOfProblemImage1 = mutableListOf<Drawable>()
        context.getDrawable(R.drawable.image1)?.let { listOfProblemImage1.add(it) }
        context.getDrawable(R.drawable.image4)?.let { listOfProblemImage1.add(it) }
        context.getDrawable(R.drawable.image3)?.let { listOfProblemImage1.add(it) }

        val listOfProblemImage2 = mutableListOf<Drawable>()
        context.getDrawable(R.drawable.image1)?.let { listOfProblemImage2.add(it) }
        context.getDrawable(R.drawable.image4)?.let { listOfProblemImage2.add(it) }
        context.getDrawable(R.drawable.image3)?.let { listOfProblemImage2.add(it) }

        var imageProblem = ListOfImages("Ac Image before service", listOfProblemImage1)
        var imageProblem2 = ListOfImages("Ac Image before service", listOfProblemImage2)

        var listofProblemImg = mutableListOf<ListOfImages>()
        listofProblemImg.add(imageProblem)
        listofProblemImg.add(imageProblem2)

        acModel = AcRepiarModel(context.getString(R.string.service_started), listOfAcRepair, listofProblemImg)
    }

    fun getAcRepairModel(): AcRepiarModel = acModel
}