package com.drivetry.acrepair.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.drivetry.acrepair.R
import com.drivetry.acrepair.model.ListOfAcRepair
import kotlinx.android.synthetic.main.item_list_ac_repair.view.*
import kotlinx.android.synthetic.main.list_ac_repair.*

class ListViewHolder(inflater: LayoutInflater, private val viewGroup: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_list_ac_repair, viewGroup, false)) {

    fun bind(repaiAc: ListOfAcRepair, lastItem: Boolean, context: Context){
        itemView.titleTv.text = repaiAc.nameAC
        itemView.priceTv.text = repaiAc.price

        if (lastItem) {
            itemView.viewLine.visibility = View.GONE
        } else {
            itemView.viewLine.visibility = View.VISIBLE
        }

        setProblemAdapter(repaiAc.listofRepair, context)
    }

    private fun setProblemAdapter(listofRepair: List<String>, context: Context) {
        val problemAdapter =
            ProblemListAdapter(listofRepair)
        itemView.repairRv.apply {
            layoutManager = GridLayoutManager(context, 3)
            adapter = problemAdapter
        }
    }

}