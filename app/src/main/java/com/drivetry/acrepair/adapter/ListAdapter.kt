package com.drivetry.acrepair.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.drivetry.acrepair.model.ListOfAcRepair

class ListAdapter(val acRepairList: List<ListOfAcRepair>, val context:Context): RecyclerView.Adapter<ListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ListViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return  acRepairList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(acRepairList[position], position == acRepairList.size-1, context)
    }
}