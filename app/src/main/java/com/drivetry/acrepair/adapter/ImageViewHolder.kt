package com.drivetry.acrepair.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.drivetry.acrepair.R
import kotlinx.android.synthetic.main.item_images.view.*

class ImageViewHolder(inflater: LayoutInflater, private val viewGroup: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_images, viewGroup, false)) {

    fun bind(drawable: Drawable){
        itemView.acImageView.setImageDrawable(drawable)
    }
}