package com.drivetry.acrepair.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.drivetry.acrepair.R
import kotlinx.android.synthetic.main.item_list_problem.view.*

class ProblemListViewHolder(inflater: LayoutInflater, private val viewGroup: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_list_problem, viewGroup, false)) {

    fun bind(problemList: String){
        itemView.itemProblemTv.text = problemList
    }
}