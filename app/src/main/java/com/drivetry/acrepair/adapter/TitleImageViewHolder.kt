package com.drivetry.acrepair.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.drivetry.acrepair.R
import com.drivetry.acrepair.model.ListOfImages
import kotlinx.android.synthetic.main.item_list_ac_repair.view.*
import kotlinx.android.synthetic.main.item_title_image.view.*

class TitleImageViewHolder(inflater: LayoutInflater, private val viewGroup: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_title_image, viewGroup, false)) {

    fun bind(titleName: ListOfImages) {
        itemView.titleImageTv.text = titleName.title
        setImageAdapter(titleName.listOfImage)
    }

    private fun setImageAdapter(listOfImage: List<Drawable>) {
        val imageAdapter = ImageAdapter(listOfImage)

        itemView.imageRv.apply {
            layoutManager = GridLayoutManager(context, 3)
            adapter = imageAdapter
        }
    }
}