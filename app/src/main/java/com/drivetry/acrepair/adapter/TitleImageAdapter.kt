package com.drivetry.acrepair.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.drivetry.acrepair.model.ListOfImages

class TitleImageAdapter(val listTitleImage: List<ListOfImages>): RecyclerView.Adapter<TitleImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TitleImageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return  TitleImageViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return listTitleImage.size
    }

    override fun onBindViewHolder(holder: TitleImageViewHolder, position: Int) {
        holder.bind(listTitleImage[position])
    }
}