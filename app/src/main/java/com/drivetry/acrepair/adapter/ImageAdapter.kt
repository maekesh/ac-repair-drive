package com.drivetry.acrepair.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ImageAdapter(val drawable: List<Drawable>):  RecyclerView.Adapter<ImageViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ImageViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return  drawable.size
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
       holder.bind(drawable[position])
    }
}