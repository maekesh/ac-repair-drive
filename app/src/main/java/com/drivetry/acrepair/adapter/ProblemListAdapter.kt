package com.drivetry.acrepair.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ProblemListAdapter(val problemList: List<String>): RecyclerView.Adapter<ProblemListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProblemListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProblemListViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return problemList.size
    }

    override fun onBindViewHolder(holder: ProblemListViewHolder, position: Int) {
        holder.bind(problemList[position])
    }
}