package com.drivetry.acrepair.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.drivetry.acrepair.model.AcRepiarModel
import com.drivetry.acrepair.usecase.MainActivityUseCase

class MainActivityViewModel : ViewModel(){
    val useCase = MainActivityUseCase()

    val getListOfRepairLiveData = MutableLiveData<AcRepiarModel>()

    fun setAcRepairList(context: Context) {
        useCase.setAcRepairModel(context)
    }

    fun getAcRepairList(){
        getListOfRepairLiveData.value = useCase.getAcRepairModel()
    }

}